MetaboClust
===========

A project to display and analyse time-course metabolomics data.

MetaboClust is a project to display and analyse time-course metabolomics data (or similar). MetaboClust focusses on the clustering pipeline and presents an interactive GUI, rather than static images. It features data correction, cluster analysis and interactive visualisation. QC-less batch correction, as described in [1,2], is supported.

**Please see the user guide for further information.**


Official website
----------------

http://software.rusilowicz.com/metaboclust


Downloads
---------

* **[Download MetaboClust 1.2.2](https://bitbucket.org/mjr129/metaboclust/downloads/MetaboClust_1_2_2.zip)**
    * *latest as of 22 August 2018*
* [User manual](https://bitbucket.org/mjr129/metaboclust/downloads/UserGuide.pdf)
* [Sample data set](https://bitbucket.org/mjr129/metaboclust/downloads/SampleData.zip)
* [Sample data tutorial](docs/tutorial.md)


License
-------

MetaboClust is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0).

Credits
-------

* Icon 'loading' from loading.io.
* Additional icons made by Freepik, Google, Appzgear, Vectorgraphit from www.flaticon.com are licensed by CC BY 3.0.

References
----------

[1] 	R. Wehrens, J. A. Hageman, F. van Eeuwijk, R. Kooke, P. J. Flood, E. Wijnker, J. J. Keurentjes, A. Lommen, H. D. van Eekelen, R. D. Hall, others, “Improved batch correction in untargeted MS-based metabolomics,” Metabolomics, vol. 12, no. 5, pp. 1–12, 2016.

[2] 	M. Rusilowicz, M. Dickinson, A. Charlton, S. O’Keefe, and J. Wilson, “A batch correction method for liquid chromatography–mass spectrometry data that does not depend on quality control samples,” Metabolomics, vol. 12, pp. 1–11, 2016.