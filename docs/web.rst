Metaboclust
===========

MetaboClust is a project to display and analyse time-course metabolomics data (or similar).
It features data correction, cluster analysis and user interactive visualisation.
Please see the user guide for further information.

.. card:: Download
    :url: https://bitbucket.org/mjr129/metaboclust/downloads/MetaboClust_1_2_2.zip
    :image: ¬file[download-photo.jpg]

.. card:: User manual
    :url: https://bitbucket.org/mjr129/metaboclust/downloads/UserGuide.pdf
    :image: ¬file[manual-photo.jpg]

.. card:: Tutorial
    :url: tutorial.md
    :image: ¬file[manual-photo.jpg]

.. card:: Sample data
    :url: https://bitbucket.org/mjr129/metaboclust/downloads/SampleData.zip
    :image: ¬file[data-photo.jpg]

.. card:: Source code
    :url: https://bitbucket.org/mjr129/metaboclust
    :image: ¬file[misc-screen.jpg]

.. card:: Back
    :url: ../../
    :image: ¬file[exit-photo.jpg]

.. alias:: ../index

.. alias:: ../default
