MetaboClust: Sample data tutorial
=================================

Introduction
------------

This documents covers, step-by-step, a simple analysis of the sample data using MetaboClust.

*Note: Details on the analysis steps themselves are not repeated here and can be found in the user guide.*

The sample data
------------------

The sample data represent a portion (a single ion mode) of the dataset described in the original paper. In summary, this data is taken from a time-course analysis of combined plant stress. Data points represent LC-MS peaks (metabolites) from samples taken from four experimental groups (Control, Drought, Fusarium and Dual-stress) over a period of two weeks. There are multiple biological replicates per experimental group, per day.

The sample data can be downloaded from the MetaboClust Bitbucket repository. Unzip the sample data before continuing.

* [Download sample data](https://bitbucket.org/mjr129/metaboclust/downloads/SampleData.zip)

The Pathway Tools databases can be downloaded from the Pathway Tools website. For the purposes of this tutorial, download the MetaCyc database. Unzip the database before continuing.

* [Download pathway tools databases](https://biocyc.org/download-flatfiles.shtml)


Configuring the software
------------------------

If MetaboClust has not been opened before then you will be prompted with the initial configuration screen. Please configure MetaboClust as outlined in the User Guide before continuing.


Loading the data
----------------

A MetaboClust "session" comprises the data and analysis. Start by creating the session.

> * Click the "create a new session button"
     
Since we've not used this data before we won't be using a previous analysis as a template.

> * Leave "blank template" selected, then click "next".
> * In the "session name" box enter a session name such as "Sample data", then click "next".
    
Now we'll select the sample data files [🖼️️️](images/01_select_data.png)

> * Set the "data source" to "negative mode".
> * Set the "data matrix" to point to the "IntensityData.csv" file in the "SampleData" folder.
> * The "observation information" should automatically point to "ObservationInfo.csv" in the "SampleData" folder.
> * The "Peak/variable information" should automatically point to "PeakInfo.csv" in the "SampleData" folder.
> * Click "next" to continue.

We'll have MetaboClust perform some analyses as it loads the data.

> * Check `specify conditions`
> * Click the button to the right of the `group(s) of interest` and choose `Both`, `Fusarium` and `Drought`.
> * Similarly, choose `Control` as the `Control group(s)`
> * Click `next` to continue
> * Check the `UV Scale and Centre` option but leave the other options in the statistics dialogue *unchecked* - we'll be creating these manually
> * Click `next` to continue.

We'll also have MetaboClust perform some rudimentary peak annotation.

> * Under `compound libraries` add the `MetaCyc` database you downloaded earlier.
>     * Note: If you put your sample library in MetaboClust's library folder it will appear in the list. If not, click the `browse for library` button and browse to the `.dat` files in the downloaded library [🖼️](images/02_select_compound_library.png).
> * Under `adduct libraries` use the `add selected library` button to add the inbuilt `refined` library.
> * Click `next` to continue.
> * Select `m/z based identification` and enter `5` ppm as the match tolerance. This is the tolerance of the acquiring LC-MS machine the data.
> * Click `next` again.

When you are ready, click `OK` to load the data. The spinning "loading" disk should appear for a moment before the main MetaboClust screen is displayed. If there are any problems you can use the `back` button to check that all the values have been entered correctly and try again.

Saving the data
---------------

To avoid having to create your session again if something goes wrong, click `save` on the main screen to save your session before continuing.

Analysing the data
------------------

Once the session is created, you can continue to explore the sample data. The remainder of this tutorial summarises the analyses which can be found described in more detail in the User Guide.

Principal components analysis
-----------------------------

You can use the PCA/MVA window to observe the batch issues with our data:

> * Click `MVA` on the main toolbar
> * In the MVA window, click `source` and change to `UV Scale and Centre`
> * Click `Legend` and change to `Batch`
> * Close the MVA window when you are done


Correct
-------

Add a batch correction routine:

> * Click `Correct` on the main toolbar to show the correction list
> * Click `New` to add a new correction
> * Set the following options:
>       * `Source`: `Original data`
>       * `Method`: `Moving median`
>       * `w`: `5`
>       * `Corrector`: `Batch`
>       * `Operator`: `Division`
>       * `Filter`: `All`

Add a "UV scaled and centre" data source to use our corrected data:

> * Select the "UV scaled and centre" item
> * Click `Edit`
> * Change the `Source` to the `Moving median` item you just created
> * Click `OK` to accept the changes
> * Click the `Down` arrow to put "UV scaled and centre" after `Moving median` in the pipeline

Commit the changes when done.

> * Click `OK` to commit the changes.
> * You can return to the PCA/MVA window to observe the effects.


Trend
-----

Add a trendline:

> * Click `Trend` on the main toolbar.
> * Click `New` to add a new trend.
> * Set the following options:
>       * `Title`: `The trend`
>       * `Source`: `UV scale and centre`
>       * `Method`: `Moving median`
>       * `w`: `5`
> * Click `OK` to accept the changes
> * Click `OK` to accept the new set of trends

Control correction
------------------

> * Click `Correct` on the main toolbar
> * Click `New`
> * Select the following options:
>       * `Title`: `Control corrected`
>       * `Method`: `Moving median`
>       * `w`: `5`
>       * `Corrector`: `Group`; `Control`
>       * `Operator`: `Subtraction`
> * Click `OK` to accept the changes
> * Click `OK` to accept the new set of corrections

Statistics
----------

Add some statistics:

> * Click `Statistics` on the main toolbar.
> * Click `New`
> * Select the following options:
>       * `Method`: `Pearson (r)`
>       * `Target`: `Control corrected `
>       * `Compare`: `Group is D`
>       * `Against`: `The corresponding time`
> * Click `OK` to accept the changes
> * Click `OK` to accept the new set of statistics

On the main screen view the statistics:

> * Under `Peak`, click `columns`
> * Check the `Pearson (r)` statistic
> * Click `OK` to hide the columns dialogue
> * Click on the `Pearson (r)` column header and select `Sort descending`
> * Double click the first peak in the list to view it
> * In the toolbar, make sure to select `Dataset`: `UV Scale and centre`
> * Observe the visible correlation with time within the "Drought" time series

Clustering
----------

> * From the menu click `Workflow | d-k-means++ wizard`
> * Select the following options:
>       * Data: `Control corrected`
>       * Experimental groups: `B, F, D`
>       * Vector: `V = {X, Y, Z}`
>       * Peaks: `All`
>       * Distance: `Euclidean`
>       * Seed: `LN1724`
>       * Clusters: `25`
>       * Examplars: `k-means`

> * Click `Cluster` in the main toolbar
> * Click `New`
> * Click the "database" icon next to `Method`
>       * Click where it says `8 hidden` to show the hidden entries
>       * Select the `d-means++` algorithm and click `unhide`
>       * Click `OK` to close the dialogue
> * Select the following options:
>       * Method: `k-means (d-means++)`
>       * Parameters: `25, 0.05, LN1724, Control, Yes`
>           * (you can click the button to the right of the parameters box to enter these in a human-readable way!)
>       * `Peaks`: `All`
>       * `Distance`: `Euclidean`
>       * `Statistics`: `None`
>       * `Vector source`: `Control corrected`
>       * `Observations`: `All`
> Click `OK` to create the clusters.

Your dataset is now complete, you can now view the clusters on the main screen.

> Click the `Clusters` tab and select `View | Popout`.
> Double click `DK11`, which should show a cluster depicting drought-response normally but not in the combined presence of *Fusarium*.

The main screen has two lists to the left, the upper list shows your entire data set, the lower lists show your data in relation to the currently selected item.

> * Click the `Pathways` tab in the lower list.
> * With `DK11` still selected in the upper list, click the `Overlapping Peaks` column and select `Sort descending`.
> * Double click the top pathway, `Cyanidin` in the lower list.
> * Peaks potentially in the `Cyanidin` pathway are now highlighted in `DK11`.

This concludes the tutorial, feel free to experiment with the sample session:

* MetaboClust is fully interactive; you can explore the overlay in more details by showing or hiding columns, or clicking individual compounds in the plots.
* You can change display options from the `Preferences` screen.
* If you don't like your batch correction, you can go back and edit your batch correction without having to worry about recreating your statistics, trend and clusters - it will all be done for you.
* For more details, see the full User Guide!
